from django.contrib import admin
from .models import (
	Booking,Room,Customer)
# Register your models here.
admin.site.register([Booking,Room,Customer])
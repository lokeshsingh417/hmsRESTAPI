from django.shortcuts import render
from rest_framework import generics
# Create your views here.
from rest_framework.permissions import IsAuthenticated
from .models import Customer
from .serializer import CustomerSerializer

class ListCreateCustomerView(generics.ListCreateAPIView):
	"""
	GET customer/
	POST customer/
	"""
	queryset = Customer.objects.all()
	serializer_class = CustomerSerializer
	permission_classes = (IsAuthenticated,)


class CustomerDetailView(generics.RetrieveUpdateDestroyAPIView):
	"""
	DELETE customer/<uuid>
	PUT customer/<uuid>
	PATCH customer/<uuid>
	GET customer/<uuid>
	"""
	queryset = Customer.objects.all()
	serializer_class = CustomerSerializer
	permission_classes = (IsAuthenticated,)


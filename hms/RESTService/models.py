from django.db import models
import uuid

class Customer(models.Model):
	customer_id = models.UUIDField(default=uuid.uuid4,primary_key=True,editable=False)
	customer_name = models.CharField(max_length=200)
	customer_address = models.TextField()
	customer_contact_no = models.CharField(max_length=10)
	customer_avator = models.ImageField(upload_to="customer/avator",blank=True,null=True)
	customer_email_id = models.EmailField()
	created_at = models.DateTimeField(auto_now_add=True)	
	def __str__(self):
		return self.customer_name

class Room(models.Model):
	room_types = (
		('D','DELUXE'),
		('SD','SUPER DELUXE'))

	room_id = models.UUIDField(default=uuid.uuid4,primary_key=True,editable=False)
	room_type = models.CharField(max_length=20,choices = room_types)
	room_pics = models.ImageField(upload_to="rooms/pic",blank=True,null=True)
	created_at = models.DateTimeField(auto_now_add=True)	
	def __str__(self):
		return self.room_type


class Booking(models.Model):
	booking_id = models.UUIDField(default=uuid.uuid4,primary_key=True,editable=False)
	customer = models.ForeignKey(Customer,on_delete=models.CASCADE) 
	room = models.ManyToManyField(Room)
	created_at=models.DateTimeField(auto_now_add=True)
	def __str__(self):
		return str(self.booking_id)

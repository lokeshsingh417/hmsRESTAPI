from django.urls import path
from .views import ListCreateCustomerView,CustomerDetailView
urlpatterns = [
path('customer/',ListCreateCustomerView.as_view(),name="list-create-customer"),
path('customer/<uuid:pk>',CustomerDetailView.as_view(),name="update-delete-customer")

]
